<?php

class Furniture implements TypeInterface{

    private $value;
    private $height;
    private $width;
    private $length;

    public function setValue($value){
        $this->value = $value;
    }

    public function getValue(){
        return "Dimension: ".$this->value;
    }

    public function getForm(){
        return "<table>
                    <tr>
                        <td class='td2'>Height</td>
                        <td class='td21'><input id='height' name='height' type='number' class='form-control'></td>
                        <td class='td211'></td>
                    </tr>
                    <tr>
                        <td class='td2'>Width</td>
                        <td class='td21'><input id='width' name='width' type='number' class='form-control'></td>
                        <td class='td211'></td>
                    </tr>
                    <tr>
                        <td class='td2'>Length</td>
                        <td class='td21'><input id='length' name='length' type='number' class='form-control'></td
                        <td class='td211'></td>
                    </tr>
                </table><br>
                Please, provide size in MB";
    }

    public function getNewValue($size, $weight, $height, $width, $length){
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
        
        if(!empty($this->height) && !empty($this->width) && !empty($this->length)){
            return $this->height."x".$this->width."x".$this->length;
        }else{
            return false;
        }
    }
}

?>