<?php
    if(isset($_POST['save'])){
        include 'database.php';
        $db = new database();

        spl_autoload_register(function($className){
            include $className.'.php';
        });

        $array_type = $db->view_type();

        $typeName = ['0'=>''];
        foreach ($array_type as $key) {
            array_push($typeName, $key['typeName']);
        }

        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $productType = $_POST['productType'];
        
        $errorEmpty = false;
        $errorSku = false;
        $errorName = false;
        $errorPrice = false;
        $errorType = false;
        $errorAttr = false;

        $type = new ProductType;
        $isClass = $type->isClass($productType);
        $newAttr = "";

        if($isClass){
            $size = (empty($_POST['size'])) ? "" : $_POST['size'];
            $weight = (empty($_POST['weight'])) ? "" : $_POST['weight'];
            $height = (empty($_POST['height'])) ? "" : $_POST['height'];
            $width = (empty($_POST['width'])) ? "" : $_POST['width'];
            $length = (empty($_POST['length'])) ? "" : $_POST['length'];

            $type->setSpecificAttribute($size, $weight, $height, $width, $length);
            $newAttr =  $type->getSpecificAttribute(new $typeName[$productType]);
            
            if(!$newAttr){
                $errorAttr = true;
            }
        }else{
            $errorType = true;
        }

        if(empty($sku) || empty($name) || empty($price) || $errorType || ($isClass && $errorAttr)){
            echo "Please, submit required data";
            $errorEmpty = true;
        }else{
            if(!$errorEmpty && !$errorSku && !$errorName && !$errorPrice && !$errorType && !$errorAttr ){
                $insert_result = $db->insert_product($sku, $name, $price, $productType, $newAttr);
                if($insert_result){
                    //
                }else{
                    echo $insert_result;
                } 
            }
        }
    }else{
        header("location: /");
    }
?>
<script>
    $("#sku, #name, #price, #productType, #size, #weight, #height, #width, #length").removeClass("input-error");
    var errorEmpty = "<?= $errorEmpty; ?>";
    var errorSku = "<?= $errorSku ?>";
    var errorName = "<?= $errorName ?>";
    var errorPrice = "<?= $errorPrice ?>";
    var errorType = "<?= $errorType ?>";
    var errorAttr = "<?= $errorAttr ?>";
    var insert_result = "<?= $insert_result ?>";

    if(insert_result==true){
        window.location.href = "/";
    }

    if(errorEmpty == true){
        $("#sku, #name, #price, #size, #weight, #height, #width, #length").addClass("input-error");
    }else{
        if(errorSku == true || insert_result != true){
            $("#sku").addClass("input-error");
        }
        if(errorName == true){
            $("#name").addClass("input-error");
        }
        if(errorPrice == true){
            $("#price").addClass("input-error");
        }
        if(errorType == true){
            $("#productType").addClass("input-error");
        }
        if(errorAttr == true){
            $("#size, #weight, #height, #width, #length").addClass("input-error");
        }
    }
</script>