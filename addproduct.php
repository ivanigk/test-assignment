<?php
	include 'database.php';
	$db = new database();

	spl_autoload_register(function($className){
		include $className.'.php';
	});

	$array_type = $db->view_type();
	
	$typeName = ['0'=>''];
	foreach ($array_type as $key) {
		array_push($typeName, $key['typeName']);
	}
	
	$return_form = ['0'=>''];
	for($i=1; $i<= count($typeName)-1; $i++){
		$type = new ProductType;
		$form = $type->setForm(new $typeName[$i]);
		array_push($return_form, $form);
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$("#product_form").submit(function(event){
				event.preventDefault();
				var sku = $("#sku").val();
				var name = $("#name").val();
				var price = $("#price").val();
				var productType = $("#productType").val();
				var save = $("#save_btn").val();
				var size = $("#size").val();
				var weight = $("#weight").val();
				var height = $("#height").val();
				var width = $("#width").val();
				var length = $("#length").val();
				$(".message").load("newproduct.php",{
					sku: sku,
					name: name,
					price: price,
					productType: productType,
					save: save,
					size: size,
					weight: weight,
					height: height,
					width: width,
					length: length
				});
			});
		});
	</script>
	<style type="text/css">
		body{
			font-size: 15px;
			margin-left: 2%;
			margin-top: 2%;
			width: 96%;
		}

		header{
			height: 10%;
		}

		main{
			height: 100%;
		}

		main .td1,.td2{
			width: 10%;
		}

		main .td11,.td21{
			width: 16%;
		}

		main .td111,.td211{
			width: 70%;
		}

		main #table{
			margin-top: ;
		}

		main #table input{
			margin-left: 1px;
			width: 215px;
		}

		footer {
			width: 96%;
			position: absolute;
			text-align: center;
			bottom: 0;
			margin-top:10%;
		}

		footer label{
			margin-bottom: 10px;
		}
		
		hr{
			align-content: center;
		}

		.input-error{
			box-shadow: 0 0 5px red;
		}

	</style>
	<title>Add Product</title>
</head>
<body>
	<form id="product_form" method="POST" action="addproduct.php">
		<header>
			<div class="btn-toolbar justify-content-between">
				<div>
					<h2>Product Add</h2>		    
				</div>
				<div class="btn-group">
					<button name="save" type="submit" id="save_btn" class="btn btn-success">Save</button>
					<label style="margin-left: 30px;"></label>
					<button type="reset" class="btn btn-secondary"><a href="/scandiweb" class="btn btn-secondary">Cancel</button></a>
				</div>				
			</div>
			<hr>
		</header>
		<main>
			<strong><p class="message" style="text-align:center; color: red;"></p></strong>
			<table class="table-borderless">
				<tr>
					<td class="td1">SKU</td>
					<td class="td11"><input type="text" name="sku" id="sku" class="form-control"></td>
					<td class="td111"></td>
				</tr>
				<tr>
					<td>Name</td>
					<td><input type="text" name="name" id="name" class="form-control"></td>
				</tr>
				<tr>
					<td>Price</td>
					<td><input type="number" name="price" id="price" class="form-control"></td>
				</tr>
				<tr>
					<td>Type Switcher</td>
					<td>
						<select name="productType" class="form-control" id="productType" onchange="showForm();">
							<option value="0">Pick One</option>
							<option value="1">DVD</option>
							<option value="2">Book</option>
							<option value="3">Furniture</option>
						</select>
					</td>
				</tr>
			</table>
			<br>
			<div id="table"></div>
		</main>
	</form>
	<footer>
		<hr>
		<label>Scandiweb Test assignment</label>
	</footer>

<script>
	<?php
		$js_array = json_encode($return_form);
		echo "var form_array = ".$js_array.";";
	?>
	function showForm(){
		$("#sku, #name, #price, #size, #weight, #height, #width, #length").removeClass("input-error");
		var x = document.getElementById("productType").value;
		document.getElementById("table").innerHTML = form_array[x];
	}	
</script>
</body>
</html>