<?php

class Dvd implements TypeInterface{

    private $value;
    private $size;

    public function setValue($value){
        $this->value = $value;
    }

    public function getValue(){
        return "Size: ".$this->value." MB";
    }

    public function getForm(){
        return "<table>
                    <tr>
                        <td class='td2'>Size</td>
                        <td class='td21'><input id='size' name='size' type='number' class='form-control'></td>
                        <td class='td211'></td>
                    </tr>
                </table><br>
                Please, provide size in MB";
    }

    public function getNewValue($size, $weight, $height, $width, $length){
        $this->size = $size;
        
        if(!empty($this->size)){
            return $this->size;
        }else{
            return false;
        }
    }
}

?>