<?php

class ProductType {

	protected $value;
    protected $className;
	
	protected $size;
	protected $weight;
	protected $height;
	protected $width;
	protected $length;

	public function getValueDB($value){
        $this->value = $value;
    }

	public function initType(TypeInterface $type){
		$type->setValue($this->value);
		echo $type->getValue();
	}

	public function setForm(TypeInterface $type){
		return $type->getForm();
	}

	public function isClass($number){
		if($number==0){
			return false;
		}else{
			return true;
		}
	}

	public function setSpecificAttribute($size,$weight,$height,$width,$length){
		$this->size = $size;
		$this->weight = $weight;
		$this->height = $height;
		$this->width = $width;
		$this->length = $length;
	}

	public function getSpecificAttribute(TypeInterface $type){
		return $type->getNewValue($this->size, $this->weight, $this->height, $this->width, $this->length);
	}
}

?>