CREATE TABLE `Product` (
  `id` int(11) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `name` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `value` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `Product`
ADD PRIMARY KEY (`id`),
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 6,
ADD UNIQUE (`sku`);

INSERT INTO `Product` (`id`, `sku`, `name`, `price`, `type`, `value`) VALUES
(1, 'sifs-dme-wdcc', 'Acme DISC', '20', 1, '500'),
(2, 'eipp-wjjz-mwiu', 'War and Rampage', '150', 2, '2'),
(3, 'posum-anda-bisa', 'War and Peace', '500', 2, '2'),
(4, 'blanc-noir-rouge', 'Chair', '125', 3, '12x22x7'),
(5, 'jssw-hmeh-wttn', 'Cupboard', '300', 3, '24x45x15');

CREATE TABLE `Type` (
  `id` int(2) NOT NULL,
  `typeName` varchar(20)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Type` (`id`, `typeName`) VALUES
(1, 'Dvd'),
(2, 'Book'),
(3, 'Furniture');

ALTER TABLE `Type`
ADD PRIMARY KEY (`id`);
