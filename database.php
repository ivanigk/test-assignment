<?php
	class database{
		private $host = "localhost";
		private $uname = "root";
		private $pass = "";
		private $db = "test";
		public $con = "";

		private $sku;
		private $name;
		private $price;
		private $type;
		private $value;
	 
		function __construct(){
			$this->con = mysqli_connect($this->host, $this->uname, $this->pass,$this->db);
		}

		function view_product(){
			$data = $this->con->query("SELECT p.id, p.sku, p.name, p.price, p.value, p.type, t.typeName FROM product p JOIN type t on p.type = t.id");
			while($d = mysqli_fetch_array($data)){
				$result[] = $d;
			}
			return $result;
		}

		function view_type(){
			$q = $this->con->query("SELECT * FROM type");
			while($d = mysqli_fetch_array($q)){
				$result[] = $d;
			}
			return $result;
		}

		function insert_product($sku, $name, $price, $type, $value){
			$stmt = $this->con->prepare("INSERT INTO product (sku, name, price, type, value) VALUES(?,?,?,?,?)");
			$stmt->bind_param("sssss", $sku, $name, $price, $type, $value);
			if(!$stmt->execute()){
				echo "Please, provide the data of indicated type";
			}else{				
				return true;
			}
		}

		function delete_product($id){
			foreach($id as $selected_id){
				$q = "DELETE from product WHERE id=".$selected_id;
				$this->con->query($q);
			}		
		}
	}
?>