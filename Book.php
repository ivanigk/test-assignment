<?php

class Book implements TypeInterface{

    private $value;
    private $weight;

    public function setValue($value){
        $this->value = $value;
    }

    public function getValue(){
        return "Weight: ".$this->value." KG";
    }

    public function getForm(){
        return "<table>
                    <tr>
                        <td class='td2'>Weight</td>
                        <td class='td21'><input id='weight' name='weight' type='number' class='form-control'></td>
                        <td class='td211'></td>
                    </tr>
                </table><br>
                Please, provide weight in KG";
    }

    public function getNewValue($size, $weight, $height, $width, $length){
        $this->weight = $weight;
        
        if(!empty($this->weight)){
            return $this->weight;
        }else{
            return false;
        }
    }
}

?>