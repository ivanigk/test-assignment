<?php
	include 'database.php';
	$db = new database();
	
	spl_autoload_register(function($className){
		include $className.'.php';
	});

	if($_SERVER["REQUEST_METHOD"] == "POST"){
		if(isset($_POST['add_product'])){
			header('location: addproduct');

		}else if(isset($_POST['mass_delete'])){
			$id = (empty($_POST['id'])) ? "" : $_POST['id'];
			if(!empty($id)){
				$db->delete_product($_POST['id']);
				header("location: /scandiweb");
			}else{
				header("location: /scandiweb");
			}			
		}
	}	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<style type="text/css">
		html{
			margin-left: 2%;
			width: 96%;
			height: 100%;
		}

		header{
			margin-top: 2%;
			height: 10%;
		}

		main{
			height: 80%;
		}

		main .row .col-sm-3{
			margin-top: 2%;
		}

		footer {
			margin-top: 5%;
			text-align: center;
			height: 10%;
			bottom: 0;
			width: 96%;
		}

		.footer-fix{			
			position: absolute;
		}
		
		.footer-flex{
			position: flex;
		}

		hr{
			align-content: center;
		}

	</style>
	<title>Product List</title>
</head>
<body>
	<form method="POST" action="index.php">
		<header>
			<div class="btn-toolbar justify-content-between">
				<div>
					<h2>Product List</h2>		    
				</div>
				<div class="btn-group">
					<button name="add_product" class="btn btn-success">ADD</button>
					<label style="margin-left: 30px;"></label>
					<button id="delete-product-btn" name="mass_delete" class="btn btn-danger">MASS DELETE</button>
				</div>
			</div>
			<hr>
		</header>

		<main>
			<div class="row">
			<?php 
				$array_product = $db->view_product();
				$ct = count($array_product);
				foreach ($array_product as $row) { ?>					
				<div class="col-sm-3">
					<div class="card">
						<div class="card-body">
						<input type="checkbox" class="delete-checkbox" name="id[]" value="<?= $row['id'];?>">
							<div style="text-align:center;">
								<span><?= $row['sku']; ?></span><br>
								<span><?= $row['name']; ?></span><br>
								<span><?= $row['price']; ?>.00 $</span><br>
								<?php
									$type = new ProductType;
									$type->getValueDB($row['value']);
								?>
								<span><?= $type->initType(new $row['typeName']); ?></span>
							</div>
						</div>
					</div>
				</div>		
			<?php } ?>
			
			</div>			
		</main>
		<footer id="footer">
			<hr>
			<label>Scandiweb Test assignment</label>
		</footer>		
	</form>
<script>
	var ct = "<?= $ct?>";
	if(ct <= 4){
		$("#footer").addClass("footer-fix");
	}else{
		$("#footer").addClass("footer-flex");
	}
</script>
</body>
</html>