<?php

interface TypeInterface {

    public function setValue($value);
    public function getValue();
    public function getForm();
    public function getNewValue($size, $weight, $height, $width, $length);
}

?>